<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
  xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <!--[if gte mso 9]>
<xml>
  <o:OfficeDocumentSettings>
  <o:AllowPNG/>
  <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
</xml>
<![endif]-->
  <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="format-detection" content="date=no" />
  <meta name="format-detection" content="address=no" />
  <meta name="format-detection" content="telephone=no" />
  <meta name="x-apple-disable-message-reformatting" />
  <!--[if !mso]><!-->
  <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400i,700,700i|Raleway:400,400i,700,700i"
    rel="stylesheet" />
  <!--<![endif]-->
  <title>Email Template</title>
  <!--[if gte mso 9]>
<style type="text/css" media="all">
  sup { font-size: 100% !important; }
</style>
<![endif]-->

  <style type="text/css" media="screen">
    /* Linked Styles */

    body {
      padding: 0 !important;
      margin: 0 !important;
      display: block !important;
      min-width: 100% !important;
      width: 100% !important;
      background: transparent;
      -webkit-text-size-adjust: none;
      font-family:Arial, sans-serif; 
      font-size:14px;
      }

    a {
      color: #4e54cb;
      text-decoration: none
    }

    p {
      padding: 0 !important;
      margin: 0 !important
    }
    img{
      width: 70px;
    }

    .emailWrapper {
      width: 650px;
      max-width: 100%;
      margin: auto;
      background: #fafafa;
    }
    .header {
      padding: 1rem;
      text-align: center;
    }
    .header h1{margin: 0px;}

    .emailBody{
      /*padding: 2rem;*/
    }

    table{width: 100%; margin-bottom: 2rem;}
    td{padding: 0.5rem 1rem; color: #2b2b2b; font-size: 14px; text-align: left;}

    .detail{
      background: #2d6ab5;
      padding: 2rem;
    }
    table.detail td{color: #fff;}
    table.detail td h4{font-size: 1.2rem; text-align: center; margin: 0px;}

    .footer{
      padding: 1rem;
      text-align: center;
      border-top: 1px solid #ccc;
    }

    .btn {
  padding: 0.8rem 1rem;
  display: inline-block;
  background: #006937;
  color: #fff;
  border-radius: 0.5rem;
  margin-top: 5rem;
  text-transform: uppercase;
  font-weight: 600;
}
.emailHeader {
  background: #006937;
  color: #fff;
  text-align: center;
  padding: 1rem;
}

    @media only screen and (max-width: 576px) {
      tbody, tr, td{
        display: block;
      }
    }


  </style>
</head>

  <body class="body">
    <div class="emailWrapper">
      <div class="header">
        <img src="https://designs.itsabacus.net/2693-invito-logistic/assets/images/logo.png" alt="Logo" style="width:200px">
      </div>
      <div class="emailBody">
        <table style="margin-bottom: 0px;">
          <tbody>
            <tr>
              <td class="emailHeader">
                <img src="email.png" alt="Icon"> 
              </td>
            </tr>
            
          </tbody>
        </table>
        <table style="margin-bottom: 0px;">
          <tbody>
            <tr>
              <td>
                <h1 style="text-align: center;">Forgot Password Recovery</h1>
                <h4 style="text-align: left;">Dear {{ $name }},</h4>
                <p style="text-align: left;">
                Your New Password is: {{$plainPassword}}</p>
                <div style="text-align: center; width: 100%;">
                </div>
              </td>
            </tr>
            
          </tbody>
        </table>
         
        <table style="margin-bottom: 0px;">
          <tbody>
            <tr> 
              <td style="text-align: center;"> Thanks </td>
            </tr>
            <tr>
              <td style="text-align: center;">
                <strong>Team Invito</strong>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="footer">
         Copyright © 2021 Invito Logistics
      </div>
    </div>    
  </body>
</html>
