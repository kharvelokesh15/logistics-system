<?php  
$newPhrase=$data->template_content;
$header_string='';
$footer_string='';
if($data->view_footer==1)
{
    $footer_string='<footer>'.$data->footer_content.'</footer>';
}
	$class_show='';
	$class_div='';
if($data->view_header==1)
{
	$header_string='<header>'.$data->header_content.'</header>';
	$class_show='header-'.$data->logo_position.'-div';
    $class_div='mt-150';
}
else
{
	$class_show='header-no-'.$data->logo_position.'-div';
	$class_div='mt-30';
}
 ?>
 
 <html>
    <head>
        <style>
            /** 
                Set the margins of the page to 0, so the footer and the header
                can be of the full height and width !
             **/
            @page {
                margin: 0cm 0cm;
            }

            /** Define now the real margins of every page in the PDF **/
            body {
                margin-top: 2cm;
                margin-left: 2cm;
                margin-right: 2cm;
                margin-bottom: 2cm;
            }

            /** Define the header rules **/
            header {
                position: fixed; 
                top: 0cm; 
                left: 0cm; 
                right: 0cm;
                /** Extra personal styles **/
                /* background-color: #000000; */
                background-color: <?= $data->header_color ?>;
                color: white;
                text-align: center;
                
            }

            /** Define the footer rules **/
            footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 3cm;

                /** Extra personal styles **/
                /* background-color: #00adef; */
                background-color: <?= $data->footer_color ?>;
                color: white;
                text-align: center;
                line-height: 0.6cm;
            }
			
			.header-center-div{
				 position: fixed; 
                left: 6cm; 
                right: 0cm;
				top:3cm;
				height: 4cm;
				height:300px; width:300px;
			}
			
			.header-left-div{
				 position: fixed; 
                left: 1cm; 
                right: 0cm;
				top:3cm;
				width:300px;
			}
			
			.header-right-div{
				 position: fixed; 
                left: 11cm; 
                right: 0cm;
				top:3cm;
				height: 4cm;
				height:300px; width:300px;
			}
			
			.header-no-center-div{
				 position: fixed; 
                left: 6cm; 
                right: 0cm;
				top:0cm;
				height: 4cm;
				height:300px; width:300px;
			}
			
			
			.header-no-left-div{
				 position: fixed; 
                left: 1cm; 
                right: 0cm;
				top:0cm;
				width:300px;
			}
			
			.header-no-right-div{
				 position: fixed; 
                left: 11cm; 
                right: 0cm;
				top:0cm;
				height: 4cm;
				height:300px; width:300px;
			}
			img{
				text-align:center; width:250px; display:inline-block;
			}
			
			.mt-150{
				display:block; 
				margin-top:150px;
			}
			
			.mt-30{
				display:block; 
				margin-top:30px;
			}
        </style>
    </head>
    <body>
        <!-- Define header and footer blocks before your content -->
      
      
             <?php  echo $header_string;?> 
        

        <!-- Wrap the content of your PDF inside a main tag -->
        <main>
		
		<div class="<?=$class_div;?>">
            <?= $newPhrase;?>
		</div>
        </main>
		
		<?php echo $footer_string;?> 
    </body>
</html>