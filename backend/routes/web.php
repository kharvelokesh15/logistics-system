<?php

use Illuminate\Http\Request;

$router->get('/', function ()
{
   // return $router->app->version();
	return "My First Lumen Response !";
});

$router->group(['prefix' => 'api'], function () use ($router) {
   // Matches "/api/register
   $router->post('register', 'UserController@register');
   $router->post('login', 'UserController@login');
});
$router->post('/user/getemail',"UserController@getemail");
$router->post('/social/register',"UserController@socialregister");
// user profile
$router->get('/userprofile/getprofile',"UserController@getUserProfile");
$router->post('/userprofile/changepass',"UserController@changePassword");
$router->post('/userprofile/editprofile',"UserController@editProfile");
$router->post('/userprofile/companyinformation',"UserController@companyInformation");
$router->post('/userprofile/upload', "UserController@upload");
$router->post('/userprofile/getuseremail', "UserController@getUserEmail");
$router->get('/userprofile/getCompanyinformation',"UserController@getCompanyInformation");
$router->post('/userprofile/getCompanyprofilepicbyid',"UserController@getCompanyProfilePicById");
$router->get('/userprofile/usersignout',"UserController@userSignout");

// geo location
$router->post('/companyprofile/companylocation',"UserController@companyLocation");
// Driver api
$router->post('/driverprofile/registerdriver',"DriverController@addDriver");
$router->get('/driverprofile/getalldriver',"DriverController@getAllDriver");
$router->post('/driverprofile/getdriverbyid',"DriverController@getDriverById");
$router->post('/driverprofile/updatedriverbyid',"DriverController@updateDriverById");
$router->post('/driverprofile/Deletedriverbyid',"DriverController@deleteDriverById");
//Customer api
$router->post('/customerprofile/addcustommer',"CustomerController@addCustomer");
$router->get('/customerprofile/getcustommer',"CustomerController@getCustomer");
$router->post('/customerprofile/deletecustommerbyid',"CustomerController@deleteCustomerById");
$router->post('/customerprofile/getcustommerbyid',"CustomerController@getCustomerById");
$router->post('/customerprofile/updatecustommerbyid',"CustomerController@updateCustomerById");
$router->post('/customerprofile/uploadcustommerimg',"CustomerController@uploadImage");
$router->post('/customerprofile/getcustommerimg',"CustomerController@getCustomerImage");
$router->post('/customerprofile/updatecustommerstatusbyid',"CustomerController@updateCustomerStatusById");
$router->post('/customerprofile/addaddresscustommerbyid',"CustomerController@addAddressCustomerById");
$router->post('/customerprofile/getcustommeraddressbyid',"CustomerController@getCustomerAddressById");
$router->post('/customerprofile/getoneaddresscustommerbyid',"CustomerController@getOneCustomerAddressById");
$router->post('/customerprofile/updateaddresscustommerbyid',"CustomerController@updateCustomerAddressById");
$router->post('/customerprofile/deleteaddresscustommerbyid',"CustomerController@deleteCustomerAddressById");
$router->post('/customerprofile/uploadcustommerdocumentbyid',"CustomerController@uploadCustomerDocumentById");
$router->post('/customerprofile/sendcustommermail',"CustomerController@sendCustomerMail");
$router->get('/customerprofile/getbranch',"CustomerController@getBranch");
$router->post('/customerprofile/getcustomeremail',"CustomerController@getCustomerEmail");
$router->post('/customerprofile/getcustomerdocumentbyid',"CustomerController@getCustomerDocumentById");
$router->post('/customerprofile/deletecustomerdocumentbyid',"CustomerController@deleteCustomerDocumentById");
$router->post('/customerprofile/downloadcustomerdocumentbyid',"CustomerController@downloadCustomerDocumentById");
$router->post('/customerprofile/changecustomerpassword',"CustomerController@changeCustomerPassword");
$router->get('/customerprofile/getactivecustomer',"CustomerController@getActiveCustomer");
// Carrier api
$router->post('/carrierprofile/addfolder', "CarrierController@createFolder");
$router->get('/carrierprofile/getfolder', "CarrierController@getFolder");
$router->post('/carrierprofile/uploaddocument', "CarrierController@uploadCarrierDoc");
$router->post('/carrierprofile/deletedocumentbyid', "CarrierController@deleteDocumentById");
$router->post('/carrierprofile/sendcarriermail', "CarrierController@carrierEmail");
$router->post('/carrier/document/pdf', "CarrierController@makePDF");
$router->get('/carrier/document/sign/document/{id}', "CarrierController@signDoc");
$router->get('/carrier/document/sign/approve/{id}', "CarrierController@approveDoc");
$router->post('/carrier/document/sign/save', "CarrierController@saveSignature");
$router->get('/carrier/getactivedrivers', "CarrierController@getActiveDrivers");
$router->post('/carrier/addcarrier', "CarrierController@addCarrier");
$router->get('/carrier/getactivecarrier', "CarrierController@getCarrier");
$router->post('/carrier/deletecarrierbyid', "CarrierController@deleteCarrierById");
$router->post('/carrier/getcarrierdatabyid', "CarrierController@getCarrierDataById");
$router->post('/carrier/updatecarrierdatabyid', "CarrierController@updateCarrierDataById");
$router->post('/carrier/deletedoclinkbycarrierid', "CarrierController@deleteDocLinkBYCarrierId");
$router->get('/carrier/getactivecarrier', "CarrierController@getActiveCarrier");
// warehouse api
$router->post('/warehouse/addproduct',"WarehouseController@addProduct");
$router->get('/warehouse/getproduct',"WarehouseController@getProduct");
$router->post('/warehouse/addwarehouse',"WarehouseController@addWarehouse");
$router->post('/warehouse/getwarehouse',"WarehouseController@getWarehouse");
$router->post('/warehouse/deletewarehousebyid',"WarehouseController@DeleteWarehouseById");
$router->post('/warehouse/getwarehousebyid',"WarehouseController@getWarehouseById");
$router->post('/warehouse/editwarehousebyid',"WarehouseController@editWarehouseById");
$router->get('/warehouse/getproductlist',"WarehouseController@getProductList");
$router->post('/warehouse/addcategory',"WarehouseController@addCategory");
$router->get('/warehouse/getallcategory',"WarehouseController@getAllCategory");
$router->post('/warehouse/deletecategorybyid',"WarehouseController@DeleteCategoryById");
$router->post('/warehouse/getcategorybyid',"WarehouseController@getCategoryById");
$router->post('/warehouse/editcategorybyid',"WarehouseController@editCategoryById");
$router->get('/warehouse/getactivecategory',"WarehouseController@getActiveCategories");
$router->post('/warehouse/csvfiledata',"WarehouseController@csvFileDataUpload");
$router->post('/warehouse/deleteproductbyid',"WarehouseController@deleteProductById");
$router->post('/warehouse/getproductbyid',"WarehouseController@getProductById");
$router->post('/warehouse/updateproductbyid',"WarehouseController@updateProductById");
$router->post('/warehouse/getproductbybranch',"WarehouseController@getProductByBranch");
//shipping center
$router->post('/shippingcenter/addshipment',"ShipmentCenterController@addShipment");
$router->get('/shippingcenter/getshipmentoceans',"ShipmentCenterController@getOceansShipment");
$router->post('/shippingcenter/deleteshipmentoceans',"ShipmentCenterController@deleteOceansShipment");
$router->post('/shippingcenter/getshipmentoceansbyid',"ShipmentCenterController@getOceansShipmentById");
$router->post('/shippingcenter/updateshipmentoceansbyid',"ShipmentCenterController@updateOceansShipmentById");
$router->post('/shippingcenter/getpackagedetailsbyid',"ShipmentCenterController@getPackageDetailsById");
$router->post('/shippingcenter/updatepackagedetailsbyid',"ShipmentCenterController@updatePackageDetailsById");
$router->post('/shippingcenter/deletepackagedetailsbyid',"ShipmentCenterController@deletePackageDetailsById");
$router->post('/shippingcenter/uploadeshipmentoceansdocbyid',"ShipmentCenterController@deletePackageDetailsById");
$router->post('/shippingcenter/uploadeshipmentoceansdocbyid',"ShipmentCenterController@deletePackageDetailsById");
$router->post('/shippingcenter/deleteshipmentoceansdocbyid',"ShipmentCenterController@deleteShipmentOceansDocById");
$router->post('/shippingcenter/updateshipmentoceanstermsbyid',"ShipmentCenterController@updateShipmentOceansTermsById");
$router->post('/shippingcenter/updateshipmentoceansnotesbyid',"ShipmentCenterController@updateShipmentOceansNotesById");
$router->post('/shippingcenter/updateshipmentchargesofoceansbyid',"ShipmentCenterController@updateShipmentChargesOceansById");
$router->post('/shippingcenter/saveshipmentchargesofoceans',"ShipmentCenterController@saveShipmentChargesOfOceans");
$router->get('/shippingcenter/getshipmentchargesofoceans',"ShipmentCenterController@getShipmentChargesOfOceans");
$router->post('/shippingcenter/addpackagechargesofoceans',"ShipmentCenterController@addPackageChargesOfOceans");
$router->post('/shippingcenter/getshipmentoceansbystatus',"ShipmentCenterController@getOceansShipmentByStatus");
//emmployee
$router->get('/employees', "EmployeeController@index");
$router->post('/employee/save', "EmployeeController@register");
$router->post('/employee/update', "EmployeeController@update");
$router->post('/employee/address/save', "EmployeeController@saveAddress");
$router->post('/employee/address/update', "EmployeeController@updateAddress");
$router->post('/employee/address/get', "EmployeeController@get_address");
$router->post('/employee/get', "EmployeeController@get");
$router->post('/employee/delete', "EmployeeController@delete");
$router->post('/employee/upload', "EmployeeController@upload");
//department
$router->get('/departments', "DepartmentController@index");
$router->post('/department/save', "DepartmentController@store");
$router->post('/department/get', "DepartmentController@get");
$router->post('/department/edit', "DepartmentController@update");
$router->post('/department/delete', "DepartmentController@delete");
//branch-location
$router->get('/branches', "BranchController@index");
$router->post('/branch/save', "BranchController@store");
$router->post('/branch/get', "BranchController@get");
$router->post('/branch/edit', "BranchController@update");
$router->post('/branch/delete', "BranchController@delete");
$router->post('/branch/upload', "BranchController@upload");
$router->post('/branch/address/save', "BranchController@saveAddress");
$router->post('/branch/address/update', "BranchController@updateAddress");
$router->post('/branch/address/get', "BranchController@get_address");
$router->post('/branch/send',"BranchController@send_email");

//Twitter
$router->get('/twitter/verify', "TwitterController@verifyCredentials");
$router->post('/twitter/profile', "TwitterController@getProfileData");