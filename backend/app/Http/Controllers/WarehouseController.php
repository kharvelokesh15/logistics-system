<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\User;
use  App\SocialMediaUsers;
use  App\CompanyInformation;
use  App\CompanyLocation;
use  App\Addresses;
use  App\Driver;
use  App\Template;
use  App\Product;
use  App\Warehouse;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class WarehouseController extends Controller
{
  public function addProduct(Request $req)
  {
    $token = JWTAuth::getToken();
    $user = JWTAuth::toUser($token); 
    $item_code = $req->input('item_code');
    $item_category = $req->input('item_category');
    $product_name = $req->input('product_name');
    $product_note = $req->input('product_note');
    $find = CompanyInformation::where('user_id', $user->id)->first();
    $addProduct = new Product;
    $addProduct->product_code = $item_code;
    $addProduct->product_category = $item_category;
    $addProduct->product_name = $product_name;
    $addProduct->product_note = $product_note;
    $addProduct->created_by = $user->id;
    $addProduct->company_id = $find->id;
    $addProduct->company_name = $find->company_name;
    $addProduct->save();
    if($addProduct)
    {
        return Json_encode(array(
            'status' => true,
            'data' => $addProduct,
        ));
    }
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "failed"
        ));
    }
  }
 public function getProduct()
 {
  $token = JWTAuth::getToken();
  $user = JWTAuth::toUser($token);
  $find = CompanyInformation::where('user_id', $user->id)->first();
  $getProduct = Product::where(['company_name'=> $find->company_name,'company_id'=>$find->id,'created_by'=> $user->id])->get();
  if($getProduct)
    {
        return Json_encode(array(
            'status' => true,
            'data' => $getProduct,
        ));
    }
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "failed"
        ));
    }
 }
 public function addWarehouse(Request $req)
 {
  $token = JWTAuth::getToken();
  $user = JWTAuth::toUser($token);
  $product_id = $req->input('product_id');
  $product_code = $req->input('product_code');
  $warehouse_location = $req->input('warehouse_location');
  $in_stock_quantity = $req->input('in_stock_quantity');
  $cost = $req->input('cost');
  $arrival_date = $req->input('arrival_date');
  $reorder_date = $req->input('reorder_date');
  $addWarehouse = new Warehouse;
  $addWarehouse->product_id = $product_id;
  $addWarehouse->product_code = $product_code;
  $addWarehouse->warehouse_location = $warehouse_location;
  $addWarehouse->in_stock_quantity = $in_stock_quantity;
  $addWarehouse->cost = $cost;
  $addWarehouse->arrival_date = $arrival_date;
  $addWarehouse->reorder_date = $reorder_date;
  $addWarehouse->created_by = $user->id;
  $addWarehouse->save();
  if($addWarehouse)
    {
        return Json_encode(array(
            'status' => true,
            'data' => $addWarehouse,
        ));
    }
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "failed"
        ));
    }
 }
 public function getWarehouse(Request $req)
 {
    $token = JWTAuth::getToken();
    $user = JWTAuth::toUser($token);
    $product_id = $req->input('product_id');
    $product_code = $req->input('product_code');
    $find = Warehouse::where(['created_by'=> $user->id,'product_id'=>$product_id,'product_code'=>$product_code])->get();
    if($find)
    {
        return Json_encode(array(
            'status' => true,
            'data' => $find,
        ));
    }
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "failed"
        ));
    }
 }
}

