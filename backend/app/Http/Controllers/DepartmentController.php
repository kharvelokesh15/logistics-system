<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\User;
use  App\Department;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
// use Mail;

class DepartmentController extends Controller
{
    public function index(Request $req)
    {


        try {
            $department = Department::all();
            return response()->json(['departments' => $department, 'message' => 'Department List Fetched Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function store(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $department = Department::create([
                'department_name' => $req->input('department_name'),
                'number_of_employees' => $req->input('number_of_employees'),
                'description' => $req->input('description'),
                'status' => $req->input('status'),
                'created_by' => $user->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            return response()->json(['message' => 'Department Stored Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }
    public function get(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $department = Department::find($req->input('id'));
            return response()->json(['department' => $department, 'message' => 'Department Detail Fetched Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function update(Request $req)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $department = Department::find($req->input('department_id'));
            $department->department_name = $req->input('department_name');
            $department->number_of_employees = $req->input('number_of_employees');
            $department->description = $req->input('description');
            $department->status = $req->input('status');
            $department->updated_by = $user->id;
            $department->updated_at = date('Y-m-d H:i:s');
            $department->save();
            return response()->json(['department' => $department, 'message' => 'Department Detail Updated Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function delete(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            Department::where('id', $req->input('department_id'))->delete();
            return response()->json(['message' => 'Department Deleted Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }
}
