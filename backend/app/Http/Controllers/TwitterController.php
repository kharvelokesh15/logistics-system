<?php

namespace App\Http\Controllers;

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
require_once base_path() . '/vendor/autoload.php';

use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use  App\SocialMediaUsers;
use  App\User;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Auth;

class TwitterController extends Controller
{

    public function verifyCredentials(Request $request)
    {
        $connection = new TwitterOAuth('b4Qom1GBQaxFNvClJRTC2ctgn', 'eMUJ1YwX00l1Auz2A3K6jQi2GiXTlEZD25Fsrmp2n3VWh6ttBr', '3001613214-SLK2euaUHxLGr6MtQG6nhLMXzsGekCP7id3UFpS', 'xZOpGQiba2WAtX87APratsb7AnjVMzEvSwlJVZvD1esc4');
        $access_token = $connection->oauth("oauth/request_token");
        $oauth_token = $access_token['oauth_token'];
        $oauth_token_secret = $access_token['oauth_token_secret'];
        SESSION::put('oauth_token_secret', $oauth_token_secret);
        return Json_encode(array(
            'status' => true,
            'redirect_url' => 'https://api.twitter.com/oauth/authorize?oauth_token=' . $access_token['oauth_token'],
            'oauth_token_secret' => $oauth_token_secret
        ));
    }

    public function getProfileData(Request $request)
    {
        $oauth_token = $request->input('oauth_token');
        $oauth_token_secret = $request->input('oauth_token_secret');

        $oauth_verifier = $request->input('oauth_verifier');
        $connection = new TwitterOAuth('b4Qom1GBQaxFNvClJRTC2ctgn', 'eMUJ1YwX00l1Auz2A3K6jQi2GiXTlEZD25Fsrmp2n3VWh6ttBr', $oauth_token, $oauth_token_secret);
        $access_token = $connection->oauth("oauth/access_token", ["oauth_verifier" => $oauth_verifier]);
        $connection = new TwitterOAuth('b4Qom1GBQaxFNvClJRTC2ctgn', 'eMUJ1YwX00l1Auz2A3K6jQi2GiXTlEZD25Fsrmp2n3VWh6ttBr', $access_token['oauth_token'], $access_token['oauth_token_secret']);
        $user = $connection->get('account/verify_credentials');
        $social_user = SocialMediaUsers::where('social_id', $user->id)->first();
        if (!$social_user) {
            $user_db = new User;
            $user_db->first_name = $user->name;
            $user_db->email = $user->screen_name . '@twitter.com';
            $user_db->password = app('hash')->make('12345678');
            $user_db->save();
            $social = new SocialMediaUsers;
            $social->user_id = $user_db->id;
            $social->social_id = $user->id;
            $social->platform = 'TWITTER';
            $social->save();
        }
        $credentials['email'] = $user->screen_name . '@twitter.com';
        $credentials['password'] = '12345678';
        $token = Auth::attempt($credentials);
        if ($token) {
            return Json_encode(array(
                'status' => true,
                'token' => $token
            ));
        }
    }
}
