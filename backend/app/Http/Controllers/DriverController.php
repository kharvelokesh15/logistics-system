<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\User;
use  App\SocialMediaUsers;
use  App\CompanyInformation;
use  App\CompanyLocation;
use  App\Addresses;
use  App\Driver;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class DriverController extends Controller
{
   public function fun1(){
    return "Controller Fun1....... ";
   }
   public function addDriver(Request $req)
   {
    $driver_lic = $req->input('driver_lic');
    $lic_state = $req->input('lic_state');
    $dob = $req->input('dob');
    $name = $req->input('name');
    $hired = $req->input('hired');
    $street_address = $req->input('address');
    $city = $req->input('city');
    $state = $req->input('state');
    $country = $req->input('country');
    $zip = $req->input('zip');
    $ss = $req->input('ss');
    $phone = $req->input('phone');
    $fax = $req->input('fax');
    $email = $req->input('email');
    $company_name = $req->input('company_name');
    $irs = $req->input('irs');
    $equip = $req->input('equip');
    $cdl_expires = $req->input('cdl_expires');
    $medcard_expires = $req->input('medcard_expires');
    $physical_expires = $req->input('physical_expires');
    $emp_type = $req->input('emp_type');
    $last_drug_test = $req->input('last_drug_test');
    $status = $req->input('status');
    $preferred = $req->input('preferred');
    $track = $req->input('track');
    $mobile_app_pass = $req->input('mobile_app_pass');
    $default_currency = $req->input('default_currency');
    $driver_code = $req->input('driver_code');
    $fule_card_no = $req->input('fule_card_no');
    $driver_terms = $req->input('driver_terms');
    $driver_instructions = $req->input('driver_instructions');
    $token = JWTAuth::getToken();
    $user = JWTAuth::toUser($token);
    $addDriver = new Driver;
    $addDriver->driver_lic=$driver_lic;
    $addDriver->lic_state=$lic_state;
    $addDriver->dob=$dob;
    $addDriver->name=$name;
    $addDriver->hired=$hired;
    $addDriver->ss=$ss;
    $addDriver->fax=$fax;
    $addDriver->company_name=$company_name;
    $addDriver->irs=$irs;
    $addDriver->equip=$equip;
    $addDriver->cdl_expires=$cdl_expires;
    $addDriver->medcard_expires=$medcard_expires;
    $addDriver->physical_expires=$physical_expires;
    $addDriver->emp_type=$emp_type;
    $addDriver->last_drug_test=$last_drug_test;
    $addDriver->status=$status;
    $addDriver->preferred=$preferred;
    $addDriver->track=$track;
    $addDriver->mobile_app_pass=$mobile_app_pass;
    $addDriver->default_currency=$default_currency;
    $addDriver->driver_code=$driver_code;
    $addDriver->fule_card_no=$fule_card_no;
    $addDriver->driver_terms=$driver_terms;
    $addDriver->driver_instructions=$driver_instructions;
    $addDriver->save();
    if ($addDriver) {
        $driverid=$addDriver->id;
        $addaddress= new Addresses;
        $addaddress->entity_id=$driverid;
        $addaddress->entity_type='Driver';
        $addaddress->email=$email;
        $addaddress->name=$name;
        $addaddress->phone=$phone;
        $addaddress->street_address=$street_address;
        $addaddress->country=$country;
        $addaddress->state=$state;
        $addaddress->city=$city;
        $addaddress->created_by=$user->id;
        $addaddress->zip=$zip;
        $addaddress->save();
        if ($addaddress) {
            return Json_encode(array(
                'status' => true,
                'data' => $addaddress
            ));
        } 
        else
        {
            return Json_encode(array(
                'status' => false,
                'massage' => "User Registration failed"
            ));
        }
    } 
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "Driver Registration failed"
        ));
    }
   }
 public function getAllDriver()
 {
    $users = DB::table('addresses')
    ->where('addresses.entity_type','Driver')
    ->leftJoin('driver', 'entity_id', '=', 'driver.id')
    ->get();
    if ($users) {
        return Json_encode(array(
            'status' => true,
            'data' => $users,
        ));
    } 
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "User Registration successful"
        ));
    }
 }
 public function getDriverById(Request $req)
 {
    $id = $req->input('id'); 
    $users = DB::table('addresses')
    ->where('addresses.entity_id', $id)
    ->leftJoin('driver', 'entity_id', '=', 'driver.id')
    ->get();
    if ($users) {
        return Json_encode(array(
            'status' => true,
            'data' => $users,
        ));
    } 
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "User Registration successful"
        ));
    }
 }
 public function updateDriverById(Request $req)
 {
    $driver_lic = $req->input('driver_lic');
    $lic_state = $req->input('lic_state');
    $dob = $req->input('dob');
    $id = $req->input('id');
    $name = $req->input('name');
    $hired = $req->input('hired');
    $street_address = $req->input('street_address');
    $city = $req->input('city');
    $state = $req->input('state');
    $country = $req->input('country');
    $zip = $req->input('zip');
    $ss = $req->input('ss');
    $phone = $req->input('phone');
    $fax = $req->input('fax');
    $email = $req->input('email');
    $company_name = $req->input('company_name');
    $irs = $req->input('irs');
    $equip = $req->input('equip');
    $cdl_expires = $req->input('cdl_expires');
    $medcard_expires = $req->input('medcard_expires');
    $physical_expires = $req->input('physical_expires');
    $emp_type = $req->input('emp_type');
    $last_drug_test = $req->input('last_drug_test');
    $status = $req->input('status');
    $preferred = $req->input('preferred');
    $track = $req->input('track');
    $mobile_app_pass = $req->input('mobile_app_pass');
    $default_currency = $req->input('default_currency');
    $driver_code = $req->input('driver_code');
    $fule_card_no = $req->input('fule_card_no');
    $driver_terms = $req->input('driver_terms');
    $driver_instructions = $req->input('driver_instructions');
    $affected = DB::table('addresses')
              ->where([['entity_id',$id],['email',$email]])
              ->update(['phone' => $phone,'street_address' => $street_address,'country' => $country,'state' => $state,'city' => $city,'zip' => $zip]);
    $driverAff = DB::table('driver')
              ->where('id',$id)
              ->update(['driver_lic' => $driver_lic,'lic_state' => $lic_state,'dob' => $dob,'name' => $name,'hired' => $hired,'ss' => $ss,'fax' => $fax,'company_name' => $company_name,'irs' => $irs,'equip' => $equip,'cdl_expires' => $cdl_expires,'medcard_expires' => $medcard_expires,'physical_expires' => $physical_expires,'emp_type' => $emp_type,'last_drug_test' => $last_drug_test,'status' => $status,'preferred' => $preferred,'track' => $track,'mobile_app_pass' => $mobile_app_pass,'default_currency' => $default_currency,'driver_code' => $driver_code,'fule_card_no' => $fule_card_no,'driver_terms' => $driver_terms,'driver_instructions' => $driver_instructions]);
    if($affected &&  $driverAff)
    {
        return Json_encode(array(
            'status' => true,
            'data1' => $affected,
            'data2' => $driverAff,
        ));
    } 
    elseif($affected || $driverAff)
    {
        return Json_encode(array(
            'status' => true,
            'data1' => $affected,
            'data2' => $driverAff,
        ));
    }
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "User successful"
        ));
    }
 }
 public function deleteDriverById(Request $req)
 {
    $id = $req->input('id');
    $deletedRows = Driver::where('id', $id)->delete();
    if ($deletedRows)
    {
        $deletedAddresses = Addresses::where('entity_id', $id)->delete();
        if ($deletedAddresses) {
            return Json_encode(array(
                'status' => true,
                'data1' => $deletedAddresses,
                'data2' => $deletedRows,
            ));
        } 
        else
        {
            return Json_encode(array(
                'status' => false,
                'massage' => "Driver DeleteAdd failed"
            ));
        }
    } 
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "Driver Delete failed"
        ));
    }
 }

}

