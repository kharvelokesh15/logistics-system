<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\User;
use  App\SocialMediaUsers;
use  App\CompanyInformation;
use  App\CompanyLocation;
use  App\Documents;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;
// use Mail;

class UserController extends Controller
{
	public function register(Request $req)
	{
		//validate incoming request 
		$this->validate($req, [
		'name' => 'required|string',
		'email' => 'required|email|unique:users',
		'password' => 'required|confirmed',
	    ]);

	try {
			$first_name = $req->input('name');
			$last_name = $req->input('last_name');
			$user_id = $req->input('user_id');
			$email = $req->input('email');
			$plainPassword = $req->input('password');
			$password = Hash::make($plainPassword);
			$company_name = $req->input('company_name');
			$company_reg_country = $req->input('company_reg_country');
			$company_legal_form = $req->input('company_legal_form');
			$company_address = $req->input('company_address');
			$country = $req->input('country');
			$state = $req->input('state');
			$phone = $req->input('phone');
			$mc = $req->input('mc');
			$dot = $req->input('dot');
			$ff = $req->input('ff');
			$user = DB::insert("insert into users(first_name,last_name,user_id,email,password,company_name,company_reg_country,company_legal_form,company_address,country,state,phone,mc,dot,ff) value(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",[$first_name,$last_name,$user_id,$email,$password,$company_name,$company_reg_country,$company_legal_form,$company_address,$country,$state,$phone,$mc,$dot,$ff]);
			if ($user) {
				$name= $first_name.' '.$last_name;
				Mail::send('mailr', compact('email','name'), function ($message) use ($email) {
                $message->to($email)->subject('Invito | Confirm register');
                });
				return Json_encode(array(
					'status' => true,
					'massage' => $user
				));
			} 
			else
			{
				return Json_encode(array(
					'status' => false,
					'massage' => "User Registration successful"
				));
			}
		
		} catch (\Exception $e)
		{
			return response()->json(['message' => 'User Registration Failed!'], 409);
		}

	}
	public function socialregister(Request $req)
	{
		$first_name = $req->input('name');
		$last_name = $req->input('last_name');
		$user_id = $req->input('user_id');
		$email = $req->input('email');
		$password = app('hash')->make('12345678');
		$company_name = $req->input('company_name');
		$company_reg_country = $req->input('company_reg_country');
		$company_legal_form = $req->input('company_legal_form');
		$company_address = $req->input('company_address');
		$country = $req->input('country');
		$state = $req->input('state');
		$phone = $req->input('phone');
		$mc = $req->input('mc');
		$dot = $req->input('dot');
		$ff = $req->input('ff');
		$social_id = $req->input('social_id');
		$platform = $req->input('platform');
		$affected = DB::table('users')
						->where('email', $email)
						->get();
		if (count($affected)>0){
			$credentials['email']=$email;
				$credentials['password']='12345678';
				$token = Auth::attempt($credentials);
			return Json_encode(array(
				'status' => true,
				'token' => $token 
			));
		} 
		else
		{
			$user=new User;
			$user->first_name=$first_name;
			$user->last_name=$last_name;
			$user->user_id=$user_id;
			$user->email=$email;
			$user->password=$password;
			$user->company_name=$company_name;
			$user->company_reg_country=$company_reg_country;
			$user->company_legal_form=$company_legal_form;
			$user->company_address=$company_address;
			$user->country=$country;
			$user->state=$state;
			$user->phone=$phone;
			$user->mc=$mc;
			$user->dot=$dot;
			$user->ff=$ff;
			$user->save();
			if ($user) {
				$users=$user->id;
				$social = new SocialMediaUsers;
				$social->user_id = $users;
				$social->social_id = $social_id;
				$social->platform = $platform;
				$social->save();
				$credentials['email']=$email;
				$credentials['password']='12345678';
				$token = Auth::attempt($credentials);
				if ($social) {
					return Json_encode(array(
						'status' => true,
						'data' => $user,
						'token'=> $token
					));
				} 
				else
				{
					return Json_encode(array(
						'status' => false,
						'massage' => "User Registration successful"
					));
				}
			} 
			else
			{
				return Json_encode(array(
					'status' => false,
					'massage' => "User Registration successful"
				));
			}
		}			  
			
	}

	public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only(['email', 'password']);

		if (! $token = Auth::attempt($credentials))
		{
			return json_encode(array(
				"status"=>false,
				"massege"=>"Unauthorized")
			);
        }
        return $this->respondWithToken($token);
	}
	
	public function getemail(Request $req)
	{
		$email= $req->input('email');
		$users = DB::table('users')
		->where('email', $email)
		->first();
		if ($users)
		{
			$plainPassword = rand(100000, 999999);
			$password = app('hash')->make($plainPassword);
			$affected = DB::table('users')
              			->where('email', $email)
						->update(['password' => $password]);
			if($affected)
			{
				$name= $users->first_name.' '.$users->last_name;
				Mail::send('mail', compact('email','name','plainPassword'), function ($message) use ($email) {
                $message->to($email)->subject('Invito | Forgot password');
            });
			return Json_encode(array(
                'status' => true,
                'data' => $affected
            ));
			}			 
		}
		else
		{
            return Json_encode(array(
                'status' => false,
                'data' => $users
            ));
        }
	}

	public function getUserProfile(Request $req)
	{
		$token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
		if($user)
		{
			return Json_encode(array(
                'status' => true,
                'data' =>  $user
            ));			 
		}
		else
		{
            return Json_encode(array(
                'status' => false,
                'data' =>  $user
            ));
        }
	}
	public function editProfile(Request $req)
	{
		$email = $req->input('email');
		$first_name = $req->input('first_name');
		$last_name = $req->input('last_name');
		$country = $req->input('country');
		$phone = $req->input('phone');
		$affected = DB::table('users')
              ->where('email', $email)
			  ->update(["first_name"=>$first_name,'last_name'=>$last_name,'country'=>$country,'phone'=>$phone]);
		if($affected)
		{
			return Json_encode(array(
				'status' => true,
				'data' =>  $affected
			));			 
		}
		else
		{
			return Json_encode(array(
				'status' => false,
				'data' =>  $affected
			));
		}
	}
	public function changePassword(Request $req)
	{
		$currentpassword = $req->input('current_password');
		$plainPassword = $req->input('password');
		$password = Hash::make($plainPassword);
		$token = JWTAuth::getToken();
		$user = JWTAuth::toUser($token);
		if( ! Hash::check($req->input('current_password'), $user->password  ))
		{
			return Json_encode(array(
				'status' => false,
				'msg' =>  'Current Password incorrect'
			));
		}
		else
		{
			$detail = User::where('email', $user->email)
			          ->update(['password' => $password]);	
			if($detail)
			{
				return Json_encode(array(
					'status' => true,
					'msg' => "password changed"
				));			 
			}
			else
			{
				return Json_encode(array(
					'status' => false,
					'msg' => "password changed failed"
				));	
			}
		}
	}
	public function companyInformation(Request $req)
	{
		$company_name = $req->input('company_name');
		$company_type = $req->input('company_type');
		$account_type = $req->input('account_type');
		$city = $req->input('city');
		$street_address = $req->input('street_address');
		$postal_code = $req->input('postal_code');
		$website = $req->input('website');
		$about = $req->input('about');
		$token = JWTAuth::getToken();
		$user = JWTAuth::toUser($token);
		$id= $user->id;
		$detail = DB::table('users')
                ->where('id', $id)
				->update(['company_name' => $company_name]);	
		$compinfo = DB::table('company_information')
				->updateOrInsert(
					['user_id' => $id],
					['company_name' => $company_name,'company_type'=>$company_type,'account_type'=>$account_type,'city'=>$city,'street_address'=>$street_address,'postal_code'=>$postal_code,'website'=>$website,'about'=>$about]
				);
		if($detail && $compinfo)
		{
			return Json_encode(array(
				'status'=>true,
				'data1'=> $compinfo,
				'data2'=> $detail)
			);			
		}
		else if($detail || $compinfo) 
		{
			return Json_encode(array(
				'status'=>true,
				'data1'=> $compinfo,
				'data2'=> $detail)
			);
		}
		else
        {
            return Json_encode(array(
                'status'=>false,
                'data1'=> $compinfo,
				'data2'=> $detail
			));
        }				
	}
	public function getCompanyInformation()
	{
		$token = JWTAuth::getToken();
		$user = JWTAuth::toUser($token);
		$id= $user->id;
		$comploc = CompanyInformation::where('user_id', $id)->get();
		if($comploc)
			{
				return Json_encode(array(
					'status'=>true,
					'data'=> $comploc)
				);
			}
			else
			{
				return Json_encode(array(
					'status'=>false,
					'compinfo'=> $comploc)
				);
			}
	}
	public function companyLocation(Request $req)
	{
		$countries = json_encode($req->input('countries'));
		$token = JWTAuth::getToken();
		$user = JWTAuth::toUser($token);
		$id= $user->id;		
		$detail = DB::table('company_information')
                ->where('user_id', $id)
				->get('id');
				
		if($detail)
		{	$com_id = $detail[0]->id;
			$comploc = new CompanyLocation;
			$comploc->company_id=$com_id;
			$comploc->user_id=$id;
			$comploc->countries=$countries;
			$comploc->save();
			if($comploc)
			{
				return Json_encode(array(
					'status'=>true,
					'data'=> $comploc)
				);
			}
			else
			{
				return Json_encode(array(
					'status'=>false,
					'compinfo'=> $comploc)
				);
			}
		}
		else
		{
			return Json_encode(array(
				'status'=>false,
				'compinfo'=> $detail)
			);
		}		
	}
	public function upload(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        // $sp_services_array = explode(',',$request->services);
        $file_url = '';
        $extension = explode("\\", $req->file);

        $ext = explode('.', $extension[2]);
        $image_parts = explode(";base64,", $req->fileSource);
        $image_base64 = base64_decode($image_parts[1]);
        // $tmpFilePath = $image_base64['tmp_name'];

        //Make sure we have a file path    
        if ($image_base64 != "") {

            $image_type = $ext[1];
            $number = mt_rand(1000000000, 9999999999);
            $fileName = 'documents' . time() . $number . '.' . $image_type;
            $newFilePath = base_path() . '/public/user_documents/' . $fileName;
            $file_url = url('user_documents') . '/' . $fileName;
            if (file_put_contents($newFilePath, $image_base64)) {
				$compinfo = DB::table('documents')
				->updateOrInsert(
					['entity_id' => $req->entity_id,'entity_type'=>$req->entity_type],
					['file_name' => $fileName,'file_type'=>$image_type,'file_address_server'=>$file_url,'status'=>1,'created_by'=>$user->id]
				);
				if($compinfo)
			{
				return Json_encode(array(
					'status'=>true,
					'data'=> $compinfo)
				);
			}
			else
			{
				return Json_encode(array(
					'status'=>false,
					'compinfo'=> $compinfo)
				);
			}
            }
        }
	}
	public function getUserEmail(Request $req)
	{
		$email = $req->input('email');
		$flight = User::where('email', $email)->first();
		if($flight)
		{
			return Json_encode(array(
				'status'=>true,
				'data'=> $flight)
			);
		}
		else
		{
			return Json_encode(array(
				'status'=>false,
				'compinfo'=> $flight)
			);
		}
	}
	public function getCompanyProfilePicById(Request $req)
	{
		$id = $req->input('id');
		$comploc = Documents::where(['entity_id'=> $id,'entity_type'=>'Company Profile Picture'])->get();
		if($comploc)
		{
			return Json_encode(array(
				'status'=>true,
				'data'=> $comploc)
			);
		}
		else
		{
			return Json_encode(array(
				'status'=>false,
				'compinfo'=> $comploc)
			);
		}
	}	
}

?>

