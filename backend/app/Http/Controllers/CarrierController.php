<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\User;
use  App\DocumentFolder;
use  App\Documents;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
// use Mail;

class CarrierController extends Controller
{
    public function index(Request $req)
    {
        try {
            $documentFolder = DocumentFolder::all();
            foreach ($documentFolder as $document) {
                $document->files = Documents::select('documents.id as id', 'entity_type', 'first_name', 'last_name', 'file_address_server', 'documents.created_at')->where('entity_id', $document->id)->where('file_name', 'Document Folder')->leftJoin('users', 'users.id', '=', 'documents.created_by')->get();
            }

            return response()->json(['documentFolders' => $documentFolder, 'message' => 'Document Folders Fetched Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function create(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $documentFolder = DocumentFolder::create([
                'folder_name' => $req->input('folder_name'),
                'created_by' => $user->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            return response()->json(['message' => 'Document Folder Stored Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function upload(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        // $sp_services_array = explode(',',$request->services);
        $file_url = '';
        $extension = explode("\\", $req->file);

        $ext = explode('.', $extension[2]);
        $image_parts = explode(";base64,", $req->fileSource);
        $image_base64 = base64_decode($image_parts[1]);
        // $tmpFilePath = $image_base64['tmp_name'];
        print_r($extension);
        //Make sure we have a file path    
        if ($image_base64 != "") {

            $image_type = $ext[1];
            $number = mt_rand(1000000000, 9999999999);
            $fileName = ($req->input('entity') != '') ? $req->input('entity') : 'documents' . time() . $number . '.' . $image_type;
            $file_name = 'documents' . time() . $number . '.' . $image_type;
            $newFilePath = base_path() . '/public/user_documents/' . $file_name;
            $file_url = url('user_documents') . '/' . $file_name;
            if (file_put_contents($newFilePath, $image_base64)) {
                $documents = new Documents;
                $documents->entity_id = $req->entity_id;
                $documents->entity_type = $req->entity_type;
                $documents->file_name = $fileName;
                $documents->file_type = $image_type;
                $documents->file_address_server = $file_url;
                $documents->status = 1;
                $documents->created_by = $user->id;
                $documents->save();
            }
        }

        return response()->json(['message' => 'All Files Uploaded Successfully!'], 200);
    }

    public function delete(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $documentFolder = Documents::where('id', $req->input('document_db_id'))->delete();
            return response()->json(['message' => 'Document File Deleted Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function email(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        $name = $user->first_name . ' ' . $user->last_name;

        try {
            $email = $req->input('email');
            $file = base_path() . '/public/user_documents/' . $req->input('file');
           
            Mail::send('document_mail', compact('name'), function ($message) use ($email, $file) {
                $message->to($email)->subject('Invito | Document');
                $message->attach($file);
            });
            return response()->json(['message' => 'Document File Emailed Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }
}
