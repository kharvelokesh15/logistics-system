<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\User;
use  App\SocialMediaUsers;
use  App\CompanyInformation;
use  App\CompanyLocation;
use  App\Addresses;
use  App\Driver;
use  App\Template;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class TemplateController extends Controller
{
  public function addTemplate(Request $req)
  {
    $token = JWTAuth::getToken();
    $user = JWTAuth::toUser($token); 
    $template_name = $req->input('template_name');
    $logo_alignment_position = $req->input('logo_alignment_position');
    $template_main_content = json_encode($req->input('template_main_content'));
    $apply_header = json_encode($req->input('apply_header'));
    $apply_footer = json_encode($req->input('apply_footer'));
    $addTemplete = new Template;
    $addTemplete->template_name = $template_name;
    $addTemplete->logo_alignment_position = $logo_alignment_position;
    $addTemplete->template_main_content = $template_main_content;
    $addTemplete->apply_header = $apply_header;
    $addTemplete->apply_footer = $apply_footer;
    $addTemplete->save();
    if($addTemplete)
    {
        return Json_encode(array(
            'status' => true,
            'data' => $addTemplete,
        ));
    }
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "failed"
        ));
    }
  }
 

}

