<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

class ExampleController extends Controller
{
   public function fun1(){
    return "Controller Fun1....... ";
   }

   public function getall()
   {
   	  $records = DB::table('user')->get();
   	  // foreach ($records as $ob)
   	  // {
   	  // 	$name[] = $ob->name;	
   	  // }
   	  // return json_encode($name);
   	  return json_encode($records);
   }
}

