<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\User;
use  App\SocialMediaUsers;
use  App\CompanyInformation;
use  App\CompanyLocation;
use  App\Addresses;
use  App\Driver;
use  App\Template;
use  App\Product;
use  App\Warehouse;
use  App\ ShippingInformation;
use  App\PackageInformation;
use  App\Customer;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class ShipmentCenterController extends Controller
{
  public function addShipment(Request $req)
  {
    $token = JWTAuth::getToken();
    $user = JWTAuth::toUser($token); 
    $customer_id = $req->input('customer_id');
    $package_information = $req->input('package_information');
    $consignee_name = $req->input('consignee_name');
    $consignee_phone = $req->input('consignee_phone');
    $consignee_email = $req->input('consignee_email');
    $consignee_street_address = $req->input('consignee_street_address');
    $consignee_country = $req->input('consignee_country');
    $consignee_state = $req->input('consignee_state');
    $consignee_city = $req->input('consignee_city');
    $consignee_zip = $req->input('consignee_zip');
    $supplier_name = $req->input('supplier_name');
    $supplier_address = $req->input('supplier_address');
    $supplier_email = $req->input('supplier_email');
    $supplier_phone = $req->input('supplier_phone');
    $supplier_Contact_name = $req->input('supplier_Contact_name');
    $supplier_Contact_tracking = $req->input('supplier_Contact_tracking');
    $commodity_type = $req->input('commodity_type');
    $commodity_customer_rate = $req->input('commodity_customer_rate');
    $commodity_carrier_rate = $req->input('commodity_carrier_rate');
    $commodity_active = $req->input('commodity_active');
    $commodity_free = $req->input('commodity_free');
    $commodity_payment_advance = $req->input('commodity_payment_advance');
    $commodity_hide_customer_invoice = $req->input('commodity_hide_customer_invoice');
    $commodity_description = $req->input('commodity_description');
    $commodity_class_name = $req->input('commodity_class_name');
    $commodity_class_customer_rate = $req->input('commodity_class_customer_rate');
    $shipping_modes = $req->input('shipping_modes');
    $shipping_tracking = $req->input('shipping_tracking');
    $shipping_suffix = $req->input('shipping_suffix');
    $shipping_Prefix = $req->input('shipping_Prefix');
    $weight_scale = $req->input('weight_scale');
    $total_weight = $req->input('total_weight');
    $shipping_length = $req->input('shipping_length');
    $shipping_width = $req->input('shipping_width');
    $shipping_height = $req->input('shipping_height');
    $shipping_booking_date = $req->input('shipping_booking_date');
    $shipping_expected_date = $req->input('shipping_expected_date');
    $shipping_mode = $req->input('shipping_mode');
    $shipping_type = $req->input('shipping_type');
    $shipping_stutus = $req->input('shipping_stutus');
    $shipping_remarks = $req->input('shipping_remarks');
    $shipping_currency = $req->input('shipping_currency');
    $shipping_payment_mode = $req->input('shipping_payment_mode');
    $shipping_payment_status = $req->input('shipping_payment_status');
    $shipping_extra_charge = $req->input('shipping_extra_charge');
    $shipping_amount = $req->input('shipping_amount');
    $name_of_charges = $req->input('name_of_charges');
    $shipping_charges_amount = $req->input('shipping_charges_amount');
    //$consignee_name = $req->input('consignee_name');
    $findCustomer = Customer::find($customer_id);
    $addShipment = new ShippingInformation;
    $addShipment->customer_id = $customer_id;
    $addShipment->customer_name =  $findCustomer->first_name +' '+ $findCustomer->last_name;
    $addShipment->consignee_name = $consignee_name;
    $addShipment->consignee_phone = $consignee_phone;
    $addShipment->consignee_email = $consignee_email;
    $addShipment->consignee_street_address = $consignee_street_address;
    $addShipment->consignee_country = $consignee_country;
    $addShipment->consignee_state = $consignee_state;
    $addShipment->consignee_city = $consignee_city;
    $addShipment->consignee_zip = $consignee_zip;
    $addShipment->supplier_name = $supplier_name;
    $addShipment->supplier_address = $supplier_address;
    $addShipment->supplier_email = $supplier_email;
    $addShipment->supplier_phone = $supplier_phone;
    $addShipment->supplier_Contact_name = $supplier_Contact_name;
    $addShipment->supplier_Contact_tracking = $supplier_Contact_tracking;
    $addShipment->commodity_type = $commodity_type;
    $addShipment->commodity_customer_rate = $commodity_customer_rate;
    $addShipment->commodity_carrier_rate = $commodity_carrier_rate;
    $addShipment->commodity_active = $commodity_active;
    $addShipment->commodity_free = $commodity_free;
    $addShipment->commodity_payment_advance = $commodity_payment_advance;
    $addShipment->commodity_hide_customer_invoice = $commodity_hide_customer_invoice;
    $addShipment->commodity_description = $commodity_description;
    $addShipment->commodity_class_name = $commodity_class_name;
    $addShipment->commodity_class_customer_rate = $commodity_class_customer_rate;
    $addShipment->shipping_modes = $shipping_modes;
    $addShipment->shipping_tracking = $shipping_tracking;
    $addShipment->shipping_suffix = $shipping_suffix;
    $addShipment->shipping_Prefix = $shipping_Prefix;
    $addShipment->weight_scale = $weight_scale;
    $addShipment->total_weight = $total_weight;
    $addShipment->shipping_length = $shipping_length;
    $addShipment->shipping_width = $shipping_width;
    $addShipment->shipping_height = $shipping_height;
    $addShipment->shipping_booking_date = $shipping_booking_date;
    $addShipment->shipping_expected_date = $shipping_expected_date;
    $addShipment->shipping_mode = $shipping_mode;
    $addShipment->shipping_type = $shipping_type;
    $addShipment->shipping_stutus = $shipping_stutus;
    $addShipment->shipping_remarks = $shipping_remarks;
    $addShipment->shipping_currency = $shipping_currency;
    $addShipment->shipping_payment_mode = $shipping_payment_mode;
    $addShipment->shipping_payment_status = $shipping_payment_status;
    $addShipment->shipping_extra_charge = $shipping_extra_charge;
    $addShipment->shipping_amount = $shipping_amount;
    $addShipment->name_of_charges = $name_of_charges;
    $addShipment->shipping_charges_amount = $shipping_charges_amount;
    $addShipment->terms_information = $terms_information;
    $addShipment->created_by =  $user->id;
    $addShipment->save();
    if($addShipment)
    {
      if($package_information)
      {
        foreach ($package_information as $package_info)
            {
                $description = $package_info['description'];
                $commodity = $package_info['commodity'];
                $weight = $package_info['weight'];
                $rate = $package_info['rate'];
                $quantity = $package_info['quantity'];
                $currency = $package_info['currency'];
                $value = $package_info['value'];
                $tax = $package_info['tax'];
                $total_value = $package_info['total_value'];                
                $addPackageInfo = new PackageInformation;
                $addPackageInfo->shipping_id = $addShipment->id;
                $addPackageInfo->description = $description;
                $addPackageInfo->commodity = $commodity;
                $addPackageInfo->weight = $weight;
                $addPackageInfo->rate = $rate;
                $addPackageInfo->quantity = $quantity;
                $addPackageInfo->currency = $currency;
                $addPackageInfo->value = $value;
                $addPackageInfo->tax = $tax;
                $addPackageInfo->total_value = $total_value;
                $addPackageInfo->created_by = $user->id;
                $addPackageInfo->save();
            }   
      }
      if($addPackageInfo && $addShipment)
      {
          return Json_encode(array(
              'status' => true,
              'data' => $addPackageInfo,
              'data' => $addShipment,
          ));
      }
      else
      {
          return Json_encode(array(
              'status' => false,
              'massage' => "failed"
          ));
      } 
    }
    if($addShipment)
    {
        return Json_encode(array(
            'status' => true,
            'data' => $addShipment,
        ));
    }
    else
    {
        return Json_encode(array(
            'status' => false,
            'massage' => "failed"
        ));
    } 
  }
}

