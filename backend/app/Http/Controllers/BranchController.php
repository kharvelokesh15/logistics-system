<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\User;
use  App\BranchLocation;
use  App\Addresses;
use  App\Documents;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
// use Mail;

class BranchController extends Controller
{
    public function index(Request $req)
    {
        try {
            $locations = BranchLocation::all();
            return response()->json(['locations' => $locations, 'message' => 'Location List Fetched Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function store(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $branch = BranchLocation::create([
                'code' => $req->input('code'),
                'number' => $req->input('number'),
                'name' => $req->input('name'),
                'created_by' => $user->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            $addresses = Addresses::create([
                'entity_id' => $branch->id,
                'entity_type' => 'Branch',
                'email' => $req->input('email'),
                'phone' => $req->input('phone'),
                'street_address' => $req->input('street_address'),
                'country' => $req->input('country'),
                'state' => $req->input('state'),
                'city' => $req->input('city'),
                'zip' => $req->input('zip'),
                'created_by' => $user->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            return response()->json(['message' => 'Branch Stored Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }
    public function get(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $location = BranchLocation::find($req->input('id'));
            $address = Addresses::where('entity_id', $req->input('id'))->where('entity_type', 'Branch')->get();
            return response()->json(['location' => $location, 'address' => $address, 'message' => 'Branch Location Detail Fetched Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function update(Request $req)
    {

        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $location = BranchLocation::find($req->input('branch_id'));

            $location->code = $req->input('code');
            $location->number = $req->input('number');
            $location->name = $req->input('name');
            $location->save();

            $addresses = Addresses::where('entity_id', $req->input('branch_id'))->where('entity_type', 'Branch')->first();
            $address = Addresses::find($addresses[0]->id);
            $address->email = $req->input('email');
            $address->phone = $req->input('phone');
            $address->street_address = $req->input('street_address');
            $address->country = $req->input('country');
            $address->state = $req->input('state');
            $address->city = $req->input('city');
            $address->zip = $req->input('zip');
            $address->updated_by = $user->id;
            $address->updated_at = date('Y-m-d H:i:s');
            $address->save();
            return response()->json(['location' => $location, 'message' => 'Branch Location Detail Updated Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function delete(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            Addresses::where('entity_id', $req->input('branch_id'))->where('entity_type', 'Branch')->delete();
            BranchLocation::where('id', $req->input('branch_id'))->delete();
            return response()->json(['message' => 'Branch Location Deleted Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function upload(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        // $sp_services_array = explode(',',$request->services);
        $file_url = '';
        $extension = explode("\\", $req->file);

        $ext = explode('.', $extension[2]);
        $image_parts = explode(";base64,", $req->fileSource);
        $image_base64 = base64_decode($image_parts[1]);
        // $tmpFilePath = $image_base64['tmp_name'];

        //Make sure we have a file path    
        if ($image_base64 != "") {

            $image_type = $ext[1];
            $number = mt_rand(1000000000, 9999999999);
            $fileName = 'documents' . time() . $number . '.' . $image_type;
            $newFilePath = base_path() . '/public/user_documents/' . $fileName;
            $file_url = url('user_documents') . '/' . $fileName;
            if (file_put_contents($newFilePath, $image_base64)) {
                $documents = new Documents;
                $documents->entity_id = $req->entity_id;
                $documents->entity_type = $req->entity_type;
                $documents->file_name = $fileName;
                $documents->file_type = $image_type;
                $documents->file_address_server = $file_url;
                $documents->status = 1;
                $documents->created_by = $user->id;
                $documents->save();
            }
        }

        return response()->json(['message' => 'All Files Uploaded Successfully!'], 200);
    }

    public function saveAddress(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $addresses = Addresses::create([
                'entity_id' => $req->input('employee_db_id'),
                'entity_type' => 'Branch',
                'name' => $req->input('name'),
                'email' => $req->input('email'),
                'phone' => $req->input('phone'),
                'street_address' => $req->input('street_address'),
                'country' => $req->input('country'),
                'state' => $req->input('state'),
                'city' => $req->input('city'),
                'zip' => $req->input('zip'),
                'ssn_tin' => $req->input('ssn_tin'),
                'created_by' => $user->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            return response()->json(['message' => 'Branch Address Saved Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function updateAddress(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $addresses = Addresses::where('id', $req->input('address_db_id'))->update([
                'entity_id' => $req->input('employee_db_id'),
                'entity_type' => 'Branch',
                'name' => $req->input('name'),
                'email' => $req->input('email'),
                'phone' => $req->input('phone'),
                'street_address' => $req->input('street_address'),
                'country' => $req->input('country'),
                'state' => $req->input('state'),
                'city' => $req->input('city'),
                'zip' => $req->input('zip'),
                'ssn_tin' => $req->input('ssn_tin'),
                'updated_by' => $user->id,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            return response()->json(['message' => 'Branch Address Saved Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function get_address(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $address = Addresses::where('id', $req->input('address_id'))->get();
            return response()->json(['address' => $address, 'message' => 'Branch Address Fetched Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function send_email(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $email = $req->input('email');
            $notes = $req->input('notes');
            Mail::send('send_mail', compact('email','notes'), function ($message) use ($email) {
                $message->to($email)->subject('Invito | Branch Mail');
            });
            return response()->json(['message' => 'Branch Email Sent Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }
}
