<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\Customer;
use  App\Documents;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
   public function fun1(){
    return "Controller Fun1....... ";
   }
   public function addCustomer(Request $req)
   {
        $account = $req->input('account');
        $first_name = $req->input('first_name');
        $last_name = $req->input('last_name');
        $email = $req->input('email');
        $Phone = $req->input('Phone');
        $branch_location = $req->input('branch_location');
        $sales_agent = $req->input('sales_agent');
        $dispatcher = $req->input('dispatcher');
        $customer_notes = $req->input('customer_notes');
        $factoring_company = $req->input('factoring_company');
        $remit_name = $req->input('remit_name');
        $remit_address = $req->input('remit_address');
        $city = $req->input('city');
        $state = $req->input('state');
        $zip_code = $req->input('zip_code');
        $remit_contact = $req->input('remit_contact');
        $remit_phone = $req->input('remit_phone');
        $remit_fax = $req->input('remit_fax');
        $payment_terms = $req->input('payment_terms');
        $local_potential = $req->input('local_potential');
        $rate_per_mile = $req->input('rate_per_mile');
        $credit_limit = $req->input('credit_limit');
        $balance = $req->input('balance');
        $available = $req->input('available');
        $customer_directions = $req->input('customer_directions');
        $carrier_notes = $req->input('carrier_notes');
        $customer_terms = $req->input('customer_terms');        
        $addCustomer = new Customer;
        $addCustomer->account=$account;
        $addCustomer->first_name=$first_name;
        $addCustomer->last_name=$last_name;
        $addCustomer->email=$email;
        $addCustomer->Phone=$Phone;
        $addCustomer->branch_location=$branch_location;
        $addCustomer->sales_agent=$sales_agent;
        $addCustomer->dispatcher=$dispatcher;
        $addCustomer->customer_notes=$customer_notes;
        $addCustomer->factoring_company=$factoring_company	;
        $addCustomer->remit_name=$remit_name;
        $addCustomer->remit_address=$remit_address;
        $addCustomer->city=$city;
        $addCustomer->state=$state;
        $addCustomer->zip_code=$zip_code;
        $addCustomer->remit_contact=$remit_contact;
        $addCustomer->remit_phone=$remit_phone;
        $addCustomer->remit_fax=$remit_fax;
        $addCustomer->payment_terms=$payment_terms	;
        $addCustomer->local_potential=$local_potential;
        $addCustomer->rate_per_mile=$rate_per_mile;
        $addCustomer->credit_limit=$credit_limit;
        $addCustomer->balance=$balance;
        $addCustomer->available=$available;
        $addCustomer->customer_directions=$customer_directions;
        $addCustomer->carrier_notes=$carrier_notes;
        $addCustomer->customer_terms=$customer_terms;
        $addCustomer->status='Active';
        $addCustomer->save();
        if ($addCustomer)
        {
            return Json_encode(array(
                'status' => true,
                'data' => $addCustomer,
            ));
        } 
        else
        {
            return Json_encode(array(
                'status' => false,
                'massage' => "Customer Registration failed"
            ));
        }
   }
   public function getCustomer()
   {
        $flights = Customer::all();
        if ($flights)
        {
            return Json_encode(array(
                'status' => true,
                'data' => $flights,
            ));
        } 
        else
        {
            return Json_encode(array(
                'status' => false,
                'massage' => "Customer Registration failed"
            ));
        }
   }
   public function deleteCustomerById(Request $req)
   {
        $id = $req->input('id');
        $deletedRows = Customer::where('id', $id)->delete();
        if ($deletedRows)
        {
            return Json_encode(array(
                'status' => true,
                'data' =>  $deletedRows,
            ));
        } 
        else
        {
            return Json_encode(array(
                'status' => false,
                'massage' => "Customer Delete failed"
            ));
        }
   }
   public function getCustomerById(Request $req)
   {
        $id = $req->input('id');
        $deletedRows = Customer::where('id', $id)->get();
        if ($deletedRows)
        {
            return Json_encode(array(
                'status' => true,
                'data' =>  $deletedRows,
            ));
        } 
        else
        {
            return Json_encode(array(
                'status' => false,
                'massage' => "Customer Delete failed"
            ));
        }
   }
   public function updateCustomerById(Request $req)
   {
    $id = $req->input('id');
    $account = $req->input('account');
    $first_name = $req->input('first_name');
    $last_name = $req->input('last_name');
    $email = $req->input('email');
    $phone = $req->input('phone');
    $branch_location = $req->input('branch_location');
    $sales_agent = $req->input('sales_agent');
    $dispatcher = $req->input('dispatcher');
    $customer_notes = $req->input('customer_notes');
    $factoring_company = $req->input('factoring_company');
    $remit_name = $req->input('remit_name');
    $remit_address = $req->input('remit_address');
    $city = $req->input('city');
    $state = $req->input('state');
    $zip_code = $req->input('zip_code');
    $remit_contact = $req->input('remit_contact');
    $remit_phone = $req->input('remit_phone');
    $remit_fax = $req->input('remit_fax');
    $payment_terms = $req->input('payment_terms');
    $local_potential = $req->input('local_potential');
    $rate_per_mile = $req->input('rate_per_mile');
    $credit_limit = $req->input('credit_limit');
    $balance = $req->input('balance');
    $available = $req->input('available');
    $customer_directions = $req->input('customer_directions');
    $carrier_notes = $req->input('carrier_notes');
    $customer_terms = $req->input('customer_terms');     
    $updateCustomer = Customer::where('id', $id)
    ->update(['account' => $account,'first_name' => $first_name,'last_name' => $last_name,'email' => $email,'phone' => $phone,'branch_location' => $branch_location,'sales_agent' => $sales_agent,'dispatcher' => $dispatcher,'customer_notes' => $customer_notes,'factoring_company' => $factoring_company,'remit_name' => $remit_name,'remit_address' => $remit_address,'city' => $city,'state' => $state,'zip_code' => $zip_code,'remit_contact' => $remit_contact,'remit_phone' => $remit_phone,'remit_fax' => $remit_fax,'payment_terms' => $payment_terms,'local_potential' => $local_potential,'rate_per_mile' => $rate_per_mile,'credit_limit' => $credit_limit,'balance' => $balance,'available' => $available,'customer_directions' => $customer_directions,'carrier_notes' => $carrier_notes,'customer_terms' => $customer_terms,'status' => 'Active']);
    if ($updateCustomer)
        {
            return Json_encode(array(
                'status' => true,
                'data' =>  $updateCustomer,
            ));
        } 
        else
        {
            return Json_encode(array(
                'status' => false,
                'massage' => "Customer Update failed"
            ));
        }
   }
   public function uploadImage(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        // $sp_services_array = explode(',',$request->services);
        $file_url = '';
        $extension = explode("\\", $req->file);

        $ext = explode('.', $extension[2]);
        $image_parts = explode(";base64,", $req->fileSource);
        $image_base64 = base64_decode($image_parts[1]);
        // $tmpFilePath = $image_base64['tmp_name'];

        //Make sure we have a file path    
        if ($image_base64 != "") {

            $image_type = $ext[1];
            $number = mt_rand(1000000000, 9999999999);
            $fileName = 'documents' . time() . $number . '.' . $image_type;
            $newFilePath = base_path() . '/public/user_documents/' . $fileName;
            $file_url = url('user_documents') . '/' . $fileName;
            if (file_put_contents($newFilePath, $image_base64)) {
                $documents = new Documents;
                $documents->entity_id = $req->entity_id;
                $documents->entity_type = $req->entity_type;
                $documents->file_name = $fileName;
                $documents->file_type = $image_type;
                $documents->file_address_server = $file_url;
                $documents->status = 1;
                $documents->created_by = $user->id;
                $documents->save();
            }
        }

        return response()->json(['message' => 'All Files Uploaded Successfully!'], 200);
    }
    public function getCustomerImage(Request $req)
    {
        $id = $req->input("id");
        $flight = Documents::where(['entity_id'=> $id,'entity_type'=>"customer Profile Picture"])->get();
        if ($flight)
        {
            return Json_encode(array(
                'status' => true,
                'data' =>  $flight,
            ));
        } 
        else
        {
            return Json_encode(array(
                'status' => false,
                'massage' => "Customer Update failed"
            ));
        }
    }
    public function updateCustomerStatusById(Request $req)
    {
        $id = $req->input("id");
        $status = $req->input("status");
        $branch = $req->input("branch");
        $update=Customer::where('id', $id)
        ->update(['status' => $status,"branch_location"=>$branch]);
        if ($update)
        {
            return Json_encode(array(
                'status' => true,
                'data' =>  $update,
            ));
        } 
        else
        {
            return Json_encode(array(
                'status' => false,
                'massage' => "Customer Update failed"
            ));
        }      
    }
}

