<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use  App\User;
use  App\Employee;
use  App\Documents;
use  App\Addresses;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\Hash;
// use Mail;

class EmployeeController extends Controller
{

    public function index(Request $req)
    {


        try {
            $employees = Employee::all();
            foreach ($employees as $employee) {
                $profile_picture = Documents::where('entity_id', $employee->id)->where('entity_type', 'Employee Profile Picture')->get();
                $employee->profile_pic = $profile_picture;
            }
            return response()->json(['employees' => $employees, 'message' => 'Employee List Fetched Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function register(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $user = new User;
            $user->first_name = $req->input('first_name');
            $user->last_name = $req->input('last_name');
            $user->email = $req->input('email');
            $plainPassword = $req->input('password');
            $password = Hash::make($plainPassword);
            $user->password = $password;
            $user->save();

            $employee = new Employee;
            $employee->user_id = $user->id;
            $employee->first_name = $req->input('first_name');
            $employee->title = $req->input('title');
            $employee->employee_id = $req->input('employee_id');
            $employee->last_name = $req->input('last_name');
            $employee->email = $req->input('email');
            $employee->phone = $req->input('phone');
            $employee->branch = $req->input('branch');
            $employee->department = $req->input('department');
            $employee->save();

            return response()->json(['message' => 'Employee Registration Successfull!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function update(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $employee = Employee::find($req->input('employee_id'));
            $employee->first_name = $req->input('first_name');
            $employee->last_name = $req->input('last_name');
            $employee->email = $req->input('email');
            $employee->phone = $req->input('phone');
            $employee->title = $req->input('title');
            $employee->alternative_number = $req->input('alternative_number');
            $employee->status = $req->input('status');
            $employee->save();

            return response()->json(['message' => 'Employee Updation Successfull!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function saveAddress(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $addresses = Addresses::create([
                'entity_id' => $req->input('employee_db_id'),
                'entity_type' => 'Employee',
                'name' => $req->input('name'),
                'email' => $req->input('email'),
                'phone' => $req->input('phone'),
                'street_address' => $req->input('street_address'),
                'country' => $req->input('country'),
                'state' => $req->input('state'),
                'city' => $req->input('city'),
                'zip' => $req->input('zip'),
                'ssn_tin' => $req->input('ssn_tin'),
                'created_by' => $user->id,
                'created_at' => date('Y-m-d H:i:s')
            ]);
            return response()->json(['message' => 'Employee Address Saved Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function updateAddress(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $addresses = Addresses::where('id', $req->input('address_db_id'))->update([
                'entity_id' => $req->input('employee_db_id'),
                'entity_type' => 'Employee',
                'name' => $req->input('name'),
                'email' => $req->input('email'),
                'phone' => $req->input('phone'),
                'street_address' => $req->input('street_address'),
                'country' => $req->input('country'),
                'state' => $req->input('state'),
                'city' => $req->input('city'),
                'zip' => $req->input('zip'),
                'ssn_tin' => $req->input('ssn_tin'),
                'updated_by' => $user->id,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            return response()->json(['message' => 'Employee Address Saved Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function get_address(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $address = Addresses::where('id', $req->input('address_id'))->get();
            return response()->json(['address' => $address, 'message' => 'Employee Address Fetched Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function get(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            $employee = Employee::find($req->input('id'));
            $address = Addresses::where('entity_id', $req->input('id'))->where('entity_type', 'Employee')->get();
            $profile_picture = Documents::where('entity_id', $req->input('id'))->where('entity_type', 'Employee Profile Picture')->get();
            $documents = Documents::where('entity_id', $req->input('id'))->where('entity_type', 'Employee Document')->get();
            return response()->json(['employee' => $employee, 'documents' => $documents, 'profile_picture' => $profile_picture, 'address' => $address, 'message' => 'Employee Detail Fetched Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function delete(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        try {
            //Addresses::where('entity_id', $req->input('branch_id'))->where('entity_type', 'Branch')->delete();
            Employee::where('id', $req->input('employee_db_id'))->delete();
            return response()->json(['message' => 'Employee Deleted Successfully!'], 200);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()], 409);
        }
    }

    public function upload(Request $req)
    {
        $token = JWTAuth::getToken();
        $user = JWTAuth::toUser($token);
        // $sp_services_array = explode(',',$request->services);
        $file_url = '';
        $extension = explode("\\", $req->file);

        $ext = explode('.', $extension[2]);
        $image_parts = explode(";base64,", $req->fileSource);
        $image_base64 = base64_decode($image_parts[1]);
        // $tmpFilePath = $image_base64['tmp_name'];

        //Make sure we have a file path    
        if ($image_base64 != "") {

            $image_type = $ext[1];
            $number = mt_rand(1000000000, 9999999999);
            $fileName = 'documents' . time() . $number . '.' . $image_type;
            $newFilePath = base_path() . '/public/user_documents/' . $fileName;
            $file_url = url('user_documents') . '/' . $fileName;
            if (file_put_contents($newFilePath, $image_base64)) {
                $documents = new Documents;
                $documents->entity_id = $req->entity_id;
                $documents->entity_type = $req->entity_type;
                $documents->file_name = $fileName;
                $documents->file_type = $image_type;
                $documents->file_address_server = $file_url;
                $documents->status = 1;
                $documents->created_by = $user->id;
                $documents->save();
            }
        }

        return response()->json(['message' => 'All Files Uploaded Successfully!'], 200);
    }
}
