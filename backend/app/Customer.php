<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class Customer extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'customer';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'account', 'first_name','last_name','email','Phone','branch_location','sales_agent','dispatcher','customer_notes','factoring_company','remit_name','remit_address','city','state','zip_code','remit_contact','remit_phone','remit_fax','payment_terms','local_potential','rate_per_mile','credit_limit','balance','available','customer_directions','carrier_notes','customer_terms','status'
    ];
}

?>