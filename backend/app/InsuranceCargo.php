<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class InsuranceCargo extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'insurance_cargo';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'carrier_id',   
        'ic_company',
        'ic_phone',
        'ic_agent',
        'ic_agent_phone',
        'ic_email',
        'ic_address',
        'ic_policy',
        'ic_expiration_date',
        'ic_limit',
        'ic_general_freight',
        'created_by',
        'updated_by',
    ];
}

?>