<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class Driver extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'driver';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'driver_lic', 'lic_state','dob','name','hired','ss','fax','company_name','irs','equip','	cdl_expires','medcard_expires','physical_expires','emp_type','last_drug_test','status','preferred','	track','mobile_app_pass','default_currency','driver_code','fule_card_no','driver_terms','driver_instructions'
    ];
}

?>