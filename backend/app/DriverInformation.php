<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class DriverInformation extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'driver_information';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
   'id', 
   'carrier_id',   
   'driver1',
   'booked_with',
   'driver_cell1',
   'booked_by',
   'driver2',
   'equipment',
   'driver_cell2',
   'rec_amt',
   'truck',
   'rec_date',
   'trailer',
   'rec_ref',
   'ref',
   'paid_amt',
   'miles',
   'paid_date',
   'deadhead_miles',
   'paid_ref',
   'created_by',
   'updated_by',
    ];
}

?>