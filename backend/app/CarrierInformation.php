<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class CarrierInformation extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'carrier_information';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'mc',
        'dot',
        'name',
        'email',
        'vendor_code',
        'scac',
        'contact_person',
        'status',
        'equip',
        'fed_tax_id',
        'track',
        'mobile_app_password',
        'contact_method',
        'default_currency',
        'carrier_terms',
        'driver_notes',
        'city',
        'country',
        'state',
        'zip',
        'phone',
        'fax',
        'document_attachment',
        'address',
        'created_by',
        'updated_by',
    ];
}

?>