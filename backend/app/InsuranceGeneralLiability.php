<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class InsuranceGeneralLiability extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'insurance_general_liability';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'carrier_id',   
        'igl_company',
        'igl_phone',
        'igl_agent',
        'igl_agent_phone',
        'igl_email',
        'igl_address',
        'igl_policy',
        'igl_expiration_date',
        'igl_limit',
        'igl_general_freight',
        'created_by',
        'updated_by',
    ];
}

?>