<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class ShippingInformation extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shipping_information';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'customer_id',
        'consignee_name',
        'consignee_phone',
        'consignee_email',
        'consignee_street_address',
        'consignee_country',
        'consignee_state',
        'consignee_city',
        'consignee_zip',
        'supplier_name',
        'supplier_address',
        'supplier_email',
        'supplier_phone',
        'supplier_Contact_name',
        'supplier_Contact_tracking',
        'commodity_type',
        'commodity_customer_rate',
        'commodity_carrier_rate',
        'commodity_active',
        'commodity_free',
        'commodity_payment_advance',
        'commodity_hide_customer_invoice',
        'commodity_description',
        'commodity_class_name',
        'commodity_class_customer_rate',
        'shipping_modes',
        'shipping_tracking',
        'shipping_suffix',
        'shipping_Prefix',
        'weight_scale',
        'total_weight',
        'shipping_length',
        'shipping_width',
        'shipping_height',
        'shipping_booking_date',
        'shipping_expected_date',
        'shipping_mode',
        'shipping_type',
        'shipping_stutus',
        'shipping_remarks',
        'shipping_currency',
        'shipping_payment_mode',
        'shipping_payment_status',
        'shipping_extra_charge',
        'shipping_amount',
        'name_of_charges',
        'shipping_charges_amount',
        'created_by',
        'created_by',
        'updated_by',
    ];
}

?>