<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class ReceiverDetail extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'receiver_detail';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'shipping_id',
        'receiver_name' ,
        'receiver_Phone',
        'receiver_email',
        'receiver_street_address',
        'receiver_country',
        'receiver_state',
        'receiver_city',
        'receiver_zip',
        'created_by',
        'updated_by',
    ];
}

?>