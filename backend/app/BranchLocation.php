<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BranchLocation extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'branch_location';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'company_id', 'code', 'number', 'name','acc_prefix','account_number','alternative_number', 'created_by',  'updated_by'
    ];
}
