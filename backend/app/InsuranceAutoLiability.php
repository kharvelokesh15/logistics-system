<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class InsuranceAutoLiability extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'insurance_auto_liability';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'carrier_id',   
        'ial_company',
        'ial_phone',
        'ial_agent',
        'ial_agent_phone',
        'ial_email',
        'ial_address',
        'ial_policy',
        'ial_expiration_date',
        'ial_limit',
        'ial_general_freight',
        'created_by',
        'updated_by',
    ];
}

?>