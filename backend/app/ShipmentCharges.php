<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class ShipmentCharges extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'shipment_charges';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'shipping_id',
        'name_of_charges',
        'amount',
        'total_amount',
        'created_by',
        'updated_by',
    ];
}

?>