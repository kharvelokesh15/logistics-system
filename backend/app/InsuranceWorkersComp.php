<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class InsuranceWorkersComp extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'insurance_workers_comp';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'carrier_id',   
        'iwc_company',
        'iwc_phone',
        'iwc_agent',
        'iwc_agent_phone',
        'iwc_email',
        'iwc_address',
        'iwc_policy',
        'iwc_expiration_date',
        'iwc_limit',
        'iwc_general_freight',
        'created_by',
        'updated_by',
    ];
}

?>