<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierAdministrative extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'carrier_administrative';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','company_id', 'folder_name', 'created_by', 'updated_by'
    ];
}
