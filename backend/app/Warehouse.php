<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class Warehouse extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'warehouse';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'product_id',   
        'warehouse_location',
        'in_stock_quantity',
        'cost',
        'arrival_date',
        'reorder_date',
        'created_by',
        'updated_by',
    ];
}

?>