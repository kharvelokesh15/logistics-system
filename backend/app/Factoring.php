<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
class Factoring extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'factoring';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'id', 
        'carrier_id',   
        'remit_name',
        'remit_city',
        'remit_state',
        'remit_zipcode',
        'remit_contact',
        'remit_phone',
        'remit_fax',
        'remit_address',
        'created_by',
        'updated_by',
    ];
}

?>