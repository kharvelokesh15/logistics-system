<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'employee';
    
    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'company_id','user_id','first_name','last_name', 'department', 'email', 'phone', 'branch', 'onboard_date', 'employee_id', 'profile_picture','title','alternative_number','status','last_login_ip','last_login_at','created_by',  'updated_by'
    ];
}